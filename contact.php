<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <title>Mon CV en ligne</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initiale-scale= 0.1">
        <meta name="presentation" content="Portfolio de Florie Anstett">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php
            include_once("header.html");
        ?>
        <nav>
            <div class="table">
                <ul>
                    <li class="menu-index"><a href="index.php">Accueil</a></li>
                    <li class="menu-exp"><a href="experiencesPro.php">Expériences professionnelles</a></li>
                    <li class="menu-hobbies"><a href="hobbies.php">Loisirs</a></li>
                    <li class="menu-contact"><a href="contact.php">Me contacter</a></li>
                </ul>
            </div>
        </nav>
        <section class="rouge">
            <h1>Me contacter</h1>
            <div class="section">
                <p>Mon profil vous intéresse ?</p>
                <p>N'hésitez pas à me contacter à l'adresse suivante :</p>
                <p><strong>florie.anstett[at]gmail.com</strong>.</p>
                <p>Je vous répondrai dans les meilleurs délais.</p>
            </div>
        </section>
        <?php 
            include_once("footer.php");
        ?>
    </body>
</html>