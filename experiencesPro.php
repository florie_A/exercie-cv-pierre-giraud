<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <title>Mon CV en ligne</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initiale-scale= 0.1">
        <meta name="description" content="Mes expériences professionnelles passées">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php
            include_once("header.html");
        ?>
        <nav>
            <div class="table">
                <ul>
                    <li class="menu-index"><a href="index.php">Accueil</a></li>
                    <li class="menu-exp"><a href="experiencesPro.php">Expériences professionnelles</a></li>
                    <li class="menu-hobbies"><a href="hobbies.php">Loisirs</a></li>
                    <li class="menu-contact"><a href="contact.php">Me contacter</a></li>
                </ul>
            </div>
        </nav>
        <section class="orange">
            <h1>Stagiaire chez Graines2tech</h1>
            <div class="section">
                <div class="leftExp">
                    <a href="https://graines2tech.is4ri.com/"><img class="logo" src="images/G2T_marque.png" alt="logo site graines2tech" /></a>
                </div>
                <div class="rightExp">
                    <p><br>Stage au sein de Graines2tech à Saverne.</p> 
                    <p>Entreprise de conseil formation sur le numérique.</p>
                    <p>Création d'une plateforme permettant de générer du code lua pour créer des mods pour le jeu
                    minetest (https://graines2blocs-minetest.is4ri.com/).</p>
                </div>
            </div>
        </section>
        <section class="rose">
            <h1>Stagiaire chez Losypamo</h1>
            <div class="section">
                <div class="leftExp">
                    <a href="http://www.losypamo.fr/"><img src="images/losypamo.jpg" alt="logo Losypamo" /></a>
                </div>
                <div class="rightExp">
                    <p>Stage chez Losypamo à Saverne.</p>
                    <p>Entreprise de développement de logiciels dédiés à la gestion de transactions bancaires et de paiements par carte.</p>
                    <p>Analyse du code existant, identification des problèmes.</p>
                    <p>Amélioration du code CSS. Maintenance d'un portail web bancaire.</p>
                </div>
            </div>
        </section>
        <?php 
            include_once("footer.php");
        ?>
    </body>
</html>