<html lang="fr-FR">
    <head>
        <title>Mon CV en ligne</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initiale-scale= 0.1">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php
            include_once("header.html");
        ?>
        <nav>
            <div class="table">
                <ul>
                    <li class="menu-index"><a href="index.php">Accueil</a></li>
                    <li class="menu-exp"><a href="experiencesPro.php">Expériences professionnelles</a></li>
                    <li class="menu-hobbies"><a href="hobbies.php">Loisirs</a></li>
                    <li class="menu-contact"><a href="contact.php">Me contacter</a></li>
                </ul>
            </div>
        </nav>
        <article>
            <section class="presentation">
                <h1>Qui suis-je ?</h1>
                <div class="section">
                    <div class="left">
                        <img class="portrait" src="images/portait.JPG" alt="photo portrait de Florie Anstett" />
                    </div>
                    <div class="right">
                        <p>Je m'appelle Florie, je suis en reconversion professionnelle.</p>
                        <p>Récemment diplômée du BTS SIO option SLAM.</p>
                        <p>Je souhaite me spécialiser en langage Javascript.</p>
                    </div>
                </div>
            </section>
            <section class="parcours">
                <h1>Parcours et expériences</h1>
                <div class="section">
                    <h2>Expériences professionnelles</h2>
                    <div class="exp">
                        <div class="left">
                            <h3>Mai - Juillet 2019</h3>
                        </div>
                        <div class="right">
                            <h3>Stagiaire chez Graines2tech</h3>
                            <p>Création d'une plateforme permettant de générer du code lua pour créer des mods pour le jeu 
                                minetest (https://graines2blocs-minetest.is4ri.com/).</p>
                        </div>
                    </div>
                    <div class="exp">
                        <div class="left">
                            <h3>Janvier - Février 2020</h3>
                        </div>
                        <div class="right">
                            <h3>Stagiaire chez Losypamo</h3>
                            <p>Analyse du code existant, identification des problèmes. Amélioration du code CSS.</p>
                            <p>Maintenance d'un portail web bancaire.</p>
                        </div>
                    </div>
                    <h2>Formation</h2>
                    <div class="exp">
                        <div class="left">
                            <h3>2018 - 2020</h3>
                        </div>
                        <div class="right">
                            <h3>BTS SIO SLAM</h3>
                            <p>Préparation et obtention du diplôme de Services Informatiques aux Organisations</p>
                            <p>option Solutions logicielles et Applications Métiers.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="competences">
                <h1>Compétences et niveau</h1>
                <div class="section">
                    <div class="cleft">
                        <div class="sous">
                            <div class="sousleft"><h3>Compétences</h3></div>
                            <div class="sousright"><h3>Niveaux</h3></div>
                        </div>
                        <div class="sous">
                            <div class="sousleft"><p>HTML / CSS</p></div>
                            <div class="sousright"><p>Intermédiaire</p></div>
                        </div>
                        <div class="sous">
                            <div class="sousleft"><p>Javascript</p></div>
                            <div class="sousright"><p>Intermédiaire</p></div>
                        </div>
                    </div>
                    <div class="cright">
                    <div class="sous">
                            <div class="sousleft"><h3>Compétences</h3></div>
                            <div class="sousright"><h3>Niveaux</h3></div>
                        </div>
                        <div class="sous">
                            <div class="sousleft"><p>PHP / SQL</p></div>
                            <div class="sousright"><p>Intermédiaire</p></div>
                        </div>
                        <div class="sous">
                            <div class="sousleft"><p>Python</p></div>
                            <div class="sousright"><p>Intermédiaire</p></div>
                        </div>
                    </div>
                </div>

            </section>
            <section class="recommendation">
                <h1>Recommendations (à télécharger)</h1>
                <div class="section">
                    <div class="reco">
                        <h2>Graines2tech</h2>
                        <div class="left">
                            <a href="" target="_blank" download="graines2tech.pdf"><img class="pdf" src="images/pdf.png" alt="logo pdf" /></a>
                        </div>
                        <div class="right">
                            <p>Florie a réalisé un excellent travail pour le site de Graines2tech.</p>
                        </div>
                    </div>
                    <div class="reco">
                        <h2>Losypamo</h2>
                        <div class="left">
                            <a href="" target="_blank" download="losypamo.pdf"><img class="pdf" src="images/pdf.png" alt="logo pdf" /></a>
                        </div>
                        <div class="right">
                            <p>Malgré l'ampleur du projet, Florie a su réaliser à bien les missions qui lui ont été confiées.</p>
                        </div>
                    </div>
                </div>
            </section>
        </article>
        <?php 
            include_once("footer.php");
        ?>
    </body>
</html>