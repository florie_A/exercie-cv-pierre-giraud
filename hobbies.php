<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <title>Mon CV en ligne</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initiale-scale= 0.1">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php
            include_once("header.html");
        ?>
        <nav>
            <div class="table">
                <ul>
                    <li class="menu-index"><a href="index.php">Accueil</a></li>
                    <li class="menu-exp"><a href="experiencesPro.php">Expériences professionnelles</a></li>
                    <li class="menu-hobbies"><a href="hobbies.php">Loisirs</a></li>
                    <li class="menu-contact"><a href="contact.php">Me contacter</a></li>
                </ul>
            </div>
        </nav>
        <section class="vert">
            <h1>Musique</h1>
            <div class="section">
                <div class="leftExp">
                    <img src="images/musique.jpg" alt="image représentant plusieurs instruments de musique" />
                </div>
                <div class="rightExp">
                    <p>Pratique du piano depuis <?php echo (date('Y') - 1996) ?> ans.</p>
                    <p> construction d'un orgue de barbarie et réalisation des arrangements et cartons.</p>
                    <p> Pratique occasionnelle de la guitare, de l'ukulele, de l'accordéon, de l'ocarina, etc.</p>
                </div>
            </section>
        </section>
        <section class="bleu">
        <h1>Photographie</h1>
            <div class="section">
                <div class="rightExp">
                    <p>Photographe amateur en macrophotographie et photos de de la faune et la flore.</p>
                </div>
                <div class="leftExp">
                    <img src="images/photographie.jpg" alt="photo d'un appreil photo numérique" />
                </div>
            </section>
        </section>
        <section class="jaune">
            <h1>Activités manuelles</h1>
            <div class="section">
                <div class="leftExp">
                    <img src="images/bricolage.jpg" alt="photo de crayons et pinceaux" />
                </div>
                <div class="rightExp">
                    <p>Activités manuelles telles que le dessin, le kirigami, la coiffure, la pyrogravure, etc.</p>
                </div>
            </section>
        </section>
        <?php 
            include_once("footer.php");
        ?>
    </body>
</html>